#!/usr/bin/python

# first, goes through bmp files and creates a new list, imagaes_sorted, that contains the file
# names of the emotions we are interested in: neutral, happy, sad, angry. Using this list, 
# parses through the bmp files, and creates a matrix of one-hot vectors with their emotion tag. 

import os
import numpy as np
import cv2
from PIL import Image 

myimages = [] #list of image filenames
dirFiles = os.listdir('.') #list of directory files
dirFiles.sort() #good initial sort but doesnt sort numerically very well
sorted(dirFiles) #sort numerically in ascending order

for files in dirFiles: #filter out all non .bmps
    if '.bmp' in files:
        myimages.append(files)


# go through the filename string names 
images_sorted = []   # initialize empty list to hold the filenames that have emotions of interest. 

for i in range(len(myimages)):         # if its not the emotions we want, don't include it  
    current_im = myimages[i]
    if (current_im[6:8] == "01" or current_im[6:8] == "03" or current_im[6:8] == "04" or current_im[6:8] == "05" or current_im[6:8] == "09"):  
        images_sorted.append(current_im)   # puts the filename 


# create a matrix [4xN] where N is number of individual image files of one hot vectors for which it is [neutral; happy; sad; angry]. Ie. so if neutral its [1;0;0;0] 
one_hot_mat = np.zeros((5,len(images_sorted))) 

for i in range(len(images_sorted)): #iterate through all of the images 
    current_im = images_sorted[i]
    
    if (current_im[6:8] == "01"):    # neutral 
        one_hot_mat[0,i] = 1 
    elif (current_im[6:8] == "03"):   # happy 
        one_hot_mat[1,i] = 1 
    elif (current_im[6:8] == "04"):   # sad 
        one_hot_mat[2,i] = 1 
    elif (current_im[6:8] == "05"):   # angry 
        one_hot_mat[3,i] = 1
    elif (current_im[6:8] == "09"):   # background noise
        one_hot_mat[4,i] = 1

one_hot_mat = one_hot_mat.transpose()  # flip rows and columns of the one hot mat to make it consistent 

#use the sorted names file to create an array filled of all the files and convert the images to black and white 
#image_files = np.array([np.array((Image.open(fname)).convert('L')) for fname in images_sorted])
image_files = np.array([cv2.imread(fname, cv2.IMREAD_GRAYSCALE).reshape(81, 150, 1) for fname in images_sorted])

print(image_files.shape)
print(one_hot_mat.shape)
        
np.savez("out.npz", sorted_emotion_filenames=images_sorted, one_hot=one_hot_mat, sorted_images = image_files)
print('saving thhe files to the current dir')