% user feedback for virtual agent (data got from excel) 


figure(); hold on

ax = gca; ax.FontSize = 20; ylabel('Response Value') 
ax.FontWeight = 'bold';
ax.LineWidth = 2; 
box(ax,'on')


xticks([1,2,3,4,5])
xticklabels({'Q1','Q2','Q3','Q4','Q5'})


mean_response = [2.777777778		2.666666667		4.333333333		4.555555556		4.777777778]; % mean velocity
std_response = [1.092906421		1.224744871		1		0.726483157		0.44095855];  % standard deviation of velocity
bar(1:5,mean_response)
errorbar(1:5,mean_response,std_response,'.','CapSize',10,'lineWidth', 5)



