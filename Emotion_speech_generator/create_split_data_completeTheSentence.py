
#!/usr/bin/env python


#split into input and target trainign data for the RNN. This will be 50-50, where the first
# part of an audio snippet is input and the next part is target. save as npz file 

import os
import numpy as np

data=np.load('/home/robert/building-interactive-machines-final-project/images_1hot_extra.npz') 

sorted_emotion_filenames = data['sorted_emotion_filenames.npy']
sorted_images = data['sorted_images.npy']

filenames_input = []   # initialize empty list to hold the filenames that will be used for training and testing 
filenames_target = []
file_input = []
file_target = []

# so gets all 01 in one vec, and all 02 or 09 in another. 
for i in range(len(sorted_emotion_filenames)):         
    current_im = sorted_emotion_filenames[i]
    if current_im[21:23] == "01":
        filenames_input.append(sorted_emotion_filenames[i])  
        filenames_target.append(sorted_emotion_filenames[i+1]) # test will always be second in sequewnce
        file_input.append(sorted_images[i])
        file_target.append(sorted_images[i+1])

np.savez("split_train_into_input_target.npz", input=file_input, target=file_target)
print('saving thhe files to the current dir')