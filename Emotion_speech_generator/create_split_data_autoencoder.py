
#!/usr/bin/env python


# split data into input and target trainign data for the autoencoder generator. First, 
# divide all indiviudal audio segemnts that have already been divided (ie. pt 01, 02, 
# 09 of a speech example into 2 parts. The fist part will be input second half will be target. 
# This also increases the amoutn of trianing data we have. 
# Also, the data size is decreased and we don't run into memory constraints. 
# used to be 150 width 81 height images. Now it will be 75 width 81 height. but double the amount. 



# last,  save as npz file 

import os
import numpy as np

data=np.load('/home/robert/building-interactive-machines-final-project/images_1hot_extra.npz') 

sorted_emotion_filenames = data['sorted_emotion_filenames.npy']
sorted_images = data['sorted_images.npy']

filenames_input = []   # initialize empty list to hold the filenames that will be used for training and testing 
filenames_target = []
file_input = []
file_target = []

regsav = []

for i in range(len(sorted_emotion_filenames)): 
    current_im = sorted_images[i]
    first_half = current_im[1:81,1:77]
    second_half = current_im[1:81,74:150]
    file_input.append(first_half)
    file_target.append(second_half)
    regsav.append(current_im)

ass = np.asarray(file_input[0])
print(ass.shape)
butt  = np.asarray(file_target[0])
print(butt.shape)


np.savez("split_train_into_input_target_Autoencoder.npz", input=file_input, target=file_target)
print('saving thhe files to the current dir')

