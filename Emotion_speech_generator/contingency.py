#!/usr/bin/env python

#instead of using trained neural network to generate sound, here is the contingency. 

#  the virtual agent classifies an emotoin it seeks through the repository of emotions and randomly strings together 
# parts of the corresponding emotion of a certain length, to generate babbling. 


import os 
import time
import numpy as np 
import cv2 
import time 
from subprocess import call 
from PIL import Image 
from PIL import ImageFilter 
import pyaudio  
import thread
import random 

def load_rand_sounds(in_emotion_tag, in_user_input_sec,in_neutral_files, in_happy_files, in_sad_files, in_angry_files):

	cut = 30   # further trim snipppets, max is 150, length dimension
	long_time = 6  # time in seconds that a user speaks will inform lenght virtual agent speaks
	short_time = 2

	rand_sounds_list = [] 

	if in_user_input_sec > long_time: #long babble ~ 5 sec
		babble_length = long_time
	else:
		babble_length = short_time

	for i in range(babble_length * 150/cut):
		rand_offset = np.random.randint(0, high=150-cut)

		if in_emotion_tag == "Happy":
			current_rand_emotion = random.choice(in_happy_files)

		elif in_emotion_tag == "Neutral":
			current_rand_emotion = random.choice(in_neutral_files)

		elif in_emotion_tag == "Angry":
			current_rand_emotion = random.choice(in_angry_files)

		elif in_emotion_tag == "Sad":
			current_rand_emotion = random.choice(in_sad_files)

		elif in_emotion_tag == "Noise":
			#dont' say anything 
			rand_sounds_list = np.zeros((1,81,150,1))
			break 
		
		current_rand_emotion = current_rand_emotion[:,rand_offset:rand_offset+cut,:]
		rand_sounds_list.append(current_rand_emotion) 

	detected_emotion = in_emotion_tag
	print("The detected Emotion was", detected_emotion)

	return rand_sounds_list 


def blend_snippets_together_and_save(in_rand_sounds_list): 

	#first, aggregate the snippets into one massive array 
	#print("The random sound shape is", np.asarray(in_rand_sounds_list).shape)

	aggregate = np.zeros((81,20,1)) # first little bit will be silence 

	for i in range(len(in_rand_sounds_list)): 
		aggregate = np.append(aggregate, in_rand_sounds_list[i],axis=1)   # stich bmp contents together horizontally 

	# process this list into an array so it can be used with PIL python package to save imag 
	aggregate = aggregate[:,:,0]

	aggregate = np.asarray(aggregate)

	aggregate = Image.fromarray(aggregate.astype('uint8'))

	#apply gaussian blur to smooth between segments 
	aggregate = aggregate.filter(ImageFilter.SMOOTH_MORE)
	aggregate = aggregate.filter(ImageFilter.SMOOTH_MORE)

	# BLUR
	# CONTOUR
	# DETAIL
	# EDGE_ENHANCE
	# EDGE_ENHANCE_MORE
	# EMBOSS
	# FIND_EDGES
	# SHARPEN
	# SMOOTH
	# SMOOTH_MORE

	#mirror image to reverese  
	#reversed_sound = aggregate.transpose(Image.FLIP_LEFT_RIGHT)

	# save image to local directory. every time new emotional speech is generated, this file is overwritten. 
	aggregate.save("gen_emotion_file.bmp")

	# convert to 24-bit colormap so ARSS can be performed on it 
	im = Image.open("gen_emotion_file.bmp").convert("RGB")

	#and resave 
	im.save("gen_emotion_file.bmp")
	return None  


def convert_to_audio(in_blended_sound_image): 

	# use arss to convert the images into audio stream to play to user  

	#call(['./arss', '-q', 'gen_emotion_file.bmp', 'gen_emotion_file.wav', '-min', '80', '-max', '20000', '--pps', '100', '-r', '44100', '-f', '16','--sine'])
	call(['./arss', '-q', 'gen_emotion_file.bmp', 'gen_emotion_file.wav', '-s', '-r', '44100', '-min', '80', '-b', '11', '-p', '150', '-f', '16'])

	return None  

def play_sounds(in_neutral_files, in_happy_files, in_sad_files, in_angry_files): 


	# wait for an emotion and seconds input: 
	#........................................
	emotion = "Angry"
	seconds = 9
	time.sleep(1)

	# load the sounds 
	sounds = load_rand_sounds(emotion, seconds,in_neutral_files, in_happy_files, in_sad_files, in_angry_files)

	# #blend the sounds together to make it sound interesting ans save to directory
	blended_sound_image = blend_snippets_together_and_save(sounds)

	# convert image to .wav file and save it to local directory
	babble = convert_to_audio(blended_sound_image)

	#play the babble sound to the user 
	os.system('python play_audio.py gen_emotion_file.wav')
	#thread.start_new_thread(os.system, ('python play_audio.py gen_emotion_file.wav',))

	print("after sound, did script pause?")

	return None  


def setup(): 

	data=np.load('images_1hot_extra.npz') 

	sorted_images = data['sorted_images.npy']
	sorted_filenames = data['sorted_emotion_filenames.npy']

	happy_files = []
	angry_files = []
	sad_files = []
	neutral_files = []

	for i in range(len(sorted_images)): #iterate through all of the images 
		current_im = sorted_images[i]
		current_filenames = sorted_filenames[i]
		
		if (current_filenames[6:8] == "01"):	# neutral 
			neutral_files.append(current_im)
		elif (current_filenames[6:8] == "03"):   # happy 
			happy_files.append(current_im)
		elif (current_filenames[6:8] == "04"):   # sad 
			sad_files.append(current_im)
		elif (current_filenames[6:8] == "05"):   # angry 
			angry_files.append(current_im)
	print('finished sorting')
	return neutral_files, happy_files, sad_files, angry_files 

def main(): 

	neutral_files, happy_files, sad_files, angry_files  = setup() # run setup and queue all emotion data 
	while(True): 

		play_sounds(neutral_files, happy_files, sad_files, angry_files)

if __name__ == "__main__":

	main()
	sys.exit(0)
