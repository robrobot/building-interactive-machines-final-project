#!/usr/bin/env python

# Script to test the output of a nerual net 

import os
import sys
import argparse
import numpy as np
import tensorflow as tf
import cv2 

from conv_autoencoder import normalize_data_per_row, load_data_from_npz_file


def load_normalization_params(norm_file):
    """
    Function to load normalization parameters
    :param norm_file: file path
    :return: mean and stdev
    """
    norm_data = np.load(norm_file)
    mean = norm_data['mean']
    stdev = norm_data['stdev']
    return mean, stdev


def main(input_file, weights_file, norm_file):
    """
    Evaluate the model on the given input data
    :param input_file: npz data
    :param weights_file: h5 file with model definition and weights
    :param norm_file: normalization params
    """
    # load data
    input, target = load_data_from_npz_file(input_file)
    N = input.shape[0]
    assert N == target.shape[0], \
        "The input and target arrays had different amounts of data ({} vs {})".format(N,
                                                                                      target.shape[0])  # sanity check!
    print("Loaded {} testing examples.".format(N))

    # load normalization params
    mean, stdev = load_normalization_params(norm_file)

    # normalize the inputs
    norm_input = normalize_data_per_row(input, mean, stdev)

    # load keras model from file
    model = tf.keras.models.load_model(weights_file)

    # set loss for evaluation
    model.compile(optimizer=tf.keras.optimizers.Adam(),
                  loss='binary_crossentropy') # accuracy threshold is 0.5


    synthesized_images = model.predict(norm_input)

    np.savez("outimages", images=synthesized_images)

    print(synthesized_images.shape)
    cv2.imshow('Test image',synthesized_images[1,:,:,:])
    cv2.waitKey(0)
    cv2.destroyAllWindows()    
    # run evaluation
    #eval_out = model.evaluate(x=norm_input, y=target, batch_size=input.shape[0])

    #print(eval_out)
    # print("Loss = {}".format(eval_out[0]))
    # print("Accuracy = {}".format(eval_out[1]))


if __name__ == "__main__":

    # script arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", help="input file (npz format)",
                        type=str, required=True)
    parser.add_argument("--logs_dir", help="logs directory",
                        type=str, required=True)
    parser.add_argument("--norm_filename", help="name for the normalization params file",
                        type=str, default="normalization_params.npz")
    parser.add_argument("--weights_filename", help="name for the weights file",
                        type=str, default="weights.h5")
    args = parser.parse_args()

    weights_path = os.path.join(args.logs_dir, args.weights_filename)
    norm_path = os.path.join(args.logs_dir, args.norm_filename)

    # run the main function
    main(args.input, weights_path, norm_path)
    sys.exit(0)