#!/usr/bin/env python


# here I will build and train the DNN by COMPLETING THE SENTENCE USING THE split_train_into_input_target.npz file 

# example terminal command: 
# python conv_autoencoder.py --input=/home/robert/building-interactive-machines-final-project/Emotion_speech_generator/split_train_into_input_target_Autoencoder.npz 
# good resources: https://machinelearningmastery.com/text-generation-lstm-recurrent-neural-networks-python-keras/
# http://babble-rnn.consected.com/docs/babble-rnn-generating-speech-from-speech-post.html


#floyd run --gpu --env tensorflow-1.3 "conv_autoencoder.py"

import cv2
import os
import sys
import argparse
import datetime
import tensorflow as tf 
from tensorflow.python.keras.layers import Flatten, Reshape
from tensorflow.python.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, LSTM ## for some reason this was trying ot get ros tf when i had tf.keras.... wtf 
import numpy as np 
import matplotlib.pyplot as plt

def load_data_from_npz_file(file_path): 

    data = np.load(file_path)
    return data['input'], data['target']   # images for input and for target 

def compute_normalization_parameters(data):  
    """
    Compute normalization parameters (mean, st. dev.)
    :param data: matrix with data organized by rows [N x num_variables]
    :return: mean and standard deviation per variable as row matrices of dimension [1 x num_variables]
    """
    mean = np.mean(data, 0)
    stdev = np.std(data, 0)

    return mean, stdev

def normalize_data_per_row(data, mean, stdev):  
    """
    Normalize a give matrix of data (samples must be organized per row)
    :param data: input data
    :param mean: mean for normalization
    :param stdev: standard deviation for normalization
    :return: whitened data, (data - mean) / stdev
    """

    # sanity checks!
    assert len(data.shape) == 4, "Expected the input data to be a 4D matrix"
    assert data.shape[1:] == mean.shape, "Data - Mean size mismatch ({} vs {})".format(data.shape[1:], mean.shape)
    assert data.shape[1:] == stdev.shape, "Data - StDev size mismatch ({} vs {})".format(data.shape[1:], stdev.shape)

    normalized_data = (data-mean)/stdev

    return data #normalized_data  


def build_model():

    # common to intiailize time 0 with vector of 0s in real time RNN 
    # to prevent the vanishing gradient problem when doing backprop for our net, we must incorporate LSTM
    # LSTM 
    # https://machinelearningmastery.com/cnn-long-short-term-memory-networks/  "MAKE A CNN LSTM model"
    # It is helpful to think of this architecture as defining two sub-models: 
    #the CNN Model for feature extraction and the LSTM Model for interpreting the features across time steps.


    input_img = tf.keras.layers.Input(shape=(80, 76, 1))  # adapt this if using `channels_first` image data format

    x = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', padding='same')(input_img)
    x = tf.keras.layers.MaxPooling2D((2, 2), padding='same')(x)
    x = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', padding='same')(x)
    encoded = tf.keras.layers.MaxPooling2D((2, 2), padding='same')(x)


    # # at this point the representation is (7, 7, 32)

    x = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', padding='same')(encoded)
    x = tf.keras.layers.UpSampling2D((2, 2))(x)
    x = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', padding='same')(x)
    x = tf.keras.layers.UpSampling2D((2, 2))(x)
    decoded = tf.keras.layers.Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)

    autoencoder = tf.keras.models.Model(input_img, decoded)

    return autoencoder  

def train_model(model, train_input, train_target, val_input, val_target, input_mean, input_stdev,
                epochs=20, learning_rate=0.01, batch_size=16): 


   # normalize
    norm_train_input = normalize_data_per_row(train_input, input_mean, input_stdev)
    norm_val_input = normalize_data_per_row(val_input, input_mean, input_stdev)


    #compile the model 
    #opt = tf.keras.optimizers.Adam(lr=1e-3, decay = 1e-5)   # start with larger learning rate but decay over itme 
    model.compile(loss='binary_crossentropy', optimizer = 'adadelta')   # trained to 2000 with adam and custom, now trying this 

    # tensorboard callback
    logs_dir = 'logs/log_{}'.format(datetime.datetime.now().strftime("%m-%d-%Y-%H-%M"))
    #logs_dir = 'logs/logNew'
    tbCallBack = tf.keras.callbacks.TensorBoard(log_dir=logs_dir, write_graph=True)

    checkpointCallBack = tf.keras.callbacks.ModelCheckpoint(os.path.join(logs_dir,'weights.h5'),
                                                            monitor='val_loss',
                                                            verbose=0,
                                                            save_best_only=True,
                                                            save_weights_only=False,
                                                            mode='auto',
                                                            period=1)

    # do training for the specified number of epochs and with the given batch size
    model.fit(norm_train_input, train_target, epochs=epochs, batch_size=batch_size,
             validation_data=(norm_val_input, val_target),
             callbacks=[tbCallBack, checkpointCallBack])
    
    #prediction = model.predict(data)

def test_model(model, test_input, test_target, input_mean, input_stdev, batch_size=60):
    """
    Test a model on a given data
    :param model: trained model to perform testing on
    :param test_input: test inputs
    :param test_target: test targets
    :param input_mean: mean for the variables in the inputs (for normalization)
    :param input_stdev: st. dev. for the variables in the inputs (for normalization)
    :return: predicted targets for the given inputs
    """
    norm_test_input = normalize_data_per_row(test_input, input_mean, input_stdev)

    prediction = model.predict(norm_test_input, batch_size = batch_size) 
    return prediction

def compute_average_L2_error(test_target, predicted_targets):
    """
    Compute the average L2 error for the predictions
    :param test_target: matrix with ground truth targets [N x 1]
    :param predicted_targets: matrix with predicted targets [N x 1]
    :return: average L2 error
    """
    average_l2_err = np.sqrt(np.sum((test_target - predicted_targets)**2))/test_target.shape[0]

    return average_l2_err

def main(npz_data_file, batch_size, epochs, lr, val, logs_dir):
    """
    Main function that performs training and test on a validation set
    :param npz_data_file: npz input file with training data
    :param batch_size: batch size to use at training time
    :param epochs: number of epochs to train for
    :param lr: learning rate
    :param val: percentage of the training data to use as validation
    :param logs_dir: directory where to save logs and trained parameters/weights
    """
    input, target = load_data_from_npz_file(npz_data_file) 

    # split this into validation and training 70 and 30 percent 

    train_input = input[0:int(0.7*len(input))]
    val_input = input[int(0.7*len(input)):len(input)]

    train_target = target[0:int(0.7*len(input))]
    val_target = target[int(0.7*len(input)):len(input)]

    N = train_input.shape[0]
    assert N == train_target.shape[0], \
        "The input and target arrays had different amounts of data ({} vs {})".format(N, train_target.shape[0]) # sanity check!
    print "Loaded {} training examples.".format(N)


    # normalize input data and save normalization parameters to file
    mean, stdev = compute_normalization_parameters(train_input)

    cv2.imshow("image", mean/255)
    print(mean)
    cv2.waitKey(0)
    cv2.destroyAllWindows() 

    # save normalization values
    np.savez(logs_dir+"/normalization_params.npz", mean=mean, stdev=stdev)

    #  call the function that builds the model
    model = build_model()

    #train the model
    print "\n\nTRAINING..."
    #train_model(model, train_input, train_target, val_input, val_target, mean, stdev,
     #          epochs=epochs, learning_rate=lr, batch_size=batch_size)

    # # test the model
    # print "\n\nTESTING..."
    # predicted_targets = test_model(model, test_input, test_target, mean, stdev)

    # # Report average L2 error
    # l2_err = compute_average_L2_error(test_target, predicted_targets)
    # print "L2 Error on Testing Set: {}".format(l2_err)

if __name__ == "__main__":

    # script arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--epochs", help="number of epochs for training",
                        type=int, default=50)
    parser.add_argument("--batch_size", help="batch size used for training",
                        type=int, default=100)
    parser.add_argument("--lr", help="learning rate for training",
                        type=float, default=1e-3)
    parser.add_argument("--val", help="percent of training data to use for validation",
                        type=float, default=0.8)
    parser.add_argument("--input", help="input file (npz format)",
                        type=str, required=True)
    parser.add_argument("--logs_dir", help="logs directory",
                        type=str, default="")
    args = parser.parse_args()

    if len(args.logs_dir) == 0: # parameter was not specified
        args.logs_dir = 'logs/log_{}'.format(datetime.datetime.now().strftime("%m-%d-%Y-%H-%M"))

    if not os.path.isdir(args.logs_dir):
        os.makedirs(args.logs_dir)

    # run the main function
    main(args.input, args.batch_size, args.epochs, args.lr, args.val, args.logs_dir)
    sys.exit(0)
