#!/usr/bin/env python

# test for display video with open cv. Can open continous, single window videos of all expressions
# based on input values coming from the DNN and the audio processing script. 
# 

import numpy as np
import cv2
import time 
import random

def main():

    run_video_and_update_animation(1) # bring up the intialization and jump into the loop 
    #print("bringing up the screen to display character animation")
    run_video_and_update_animation(0)  

def init_emotion_tag(): # initialize character with neutral expression and not talking
    return "Sad"

def init_talk_state():
    return "No_Talk" 

def get_emotion_tag(): 

    # test code to randomly generate emotions, and then keep them for a while, to see if stuff works 

    emotion = "Angry"
    return emotion 

def get_talk_state(): 
    # write the code to interface with RNN babbling generation to move character mouth only when speak
    return "No_Talk"


def load_face_animation(string, string_2):
    if string == "Angry" and string_2 == "No_Talk":
        cap = cv2.VideoCapture('Angry_No_Talk_TRIMMED.mp4')
    elif string == "Angry" and string_2 == "Talk": 
        cap = cv2.VideoCapture('Angry_babble_TRIMMED.mp4')
    elif string == "Happy" and string_2 == "No_Talk":
        cap = cv2.VideoCapture('Angry_No_Talk_TRIMMED.mp4')
    elif string == "Happy" and string_2 == "Talk": 
        cap = cv2.VideoCapture('Angry_babble_TRIMMED.mp4')
    elif string == "Sad" and string_2 == "No_Talk":
        cap = cv2.VideoCapture('Sad_no_talk_Trimmed.mp4')
    elif string == "Sad" and string_2 == "Talk": 
        cap = cv2.VideoCapture('Sad_babble_trimmed.mp4')
    elif string == "Neutral" and string_2 == "No_Talk":
        cap = cv2.VideoCapture('Neutral_no_talk_Trimmed.mp4')
    elif string == "Neutral" and string_2 == "Talk": 
        cap = cv2.VideoCapture('Neutral_babble_trimmed.mp4')
    else: 
        print("not a recognized emotion")    


    return cap 

def run_video_and_update_animation(in_init_or_not): # input is 1 if its first time its called. 0 otherwisze

    if in_init_or_not == 1: 

        emotion_tag = init_emotion_tag() # initialize emotion tag and talk state with neutral, no talk
        talk_state = init_talk_state() 
        cap = load_face_animation(emotion_tag, talk_state)
        time.sleep(0.5) # delay while the window and character intialize 


    else: # we're past initialization, now lets continuously update the face in a loop with different emotions 

        while(True): 
            old_emotion_tag = get_emotion_tag()

            old_talk_state = get_talk_state() 

            cap = load_face_animation(old_emotion_tag, old_talk_state) # update the face animation based on present emotion
            frame_counter = 0        

            # Check if camera opened successfully
            if (cap.isOpened()== False): 
              print("Error opening video file")

            while(True):   

                emotion_tag = get_emotion_tag() # constantly search for emotion tag and talk state to see if it's changed
                talk_state = get_talk_state() 

                # if the old emotion tag and talk state are the same as the present one, continue
                if old_emotion_tag == emotion_tag and old_talk_state == talk_state:  

                    ret, frame = cap.read() 
                    frame_counter += 1 # iterate frame counter 
                    if ret == True:
                     
                        # Display the resulting frame
                        cv2.imshow('Frame',frame)
                        cv2.waitKey(30)
                        #print("displaying frame",frame_counter, "of", cap.get(cv2.CAP_PROP_FRAME_COUNT))

                else:   # if old tags and current are not the same, there's been a change, so break the while loop and redo the load_face_animation 
                    print('Chage in state from:', old_emotion_tag, old_talk_state,'to:', emotion_tag, talk_state)
                    break 


                #If the last frame is reached the video has ended, so reset the frame to 0 in reset the counter
                if frame_counter == cap.get(cv2.CAP_PROP_FRAME_COUNT):
                    frame_counter = 0 
                    cap.set(cv2.CAP_PROP_POS_FRAMES, 0)


            # Press Q on keyboard to  destroy character  
            if cv2.waitKey(25) & 0xFF == ord('q'):
                cap.release()
                cv2.destroyAllWindows()



if __name__ == "__main__":
    
    # run the main function
    main()
    sys.exit(0)