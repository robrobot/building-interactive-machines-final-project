#!/usr/bin/env python
# PyAudio example: Record a few seconds of audio and save to a WAVE file.

import pyaudio
import wave
import cv2
from subprocess import call, Popen, PIPE
import numpy as np
import re

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 44100
RECORD_SECONDS = 1
WAVE_OUTPUT_FILENAME = "output.wav"

p = pyaudio.PyAudio()

Number = 10

while(True):

	stream = p.open(format=FORMAT,
	                channels=CHANNELS,
	                rate=RATE,
	                input=True,
	                frames_per_buffer=CHUNK)

	print("* recording")

	frames = []

	for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
	    data = stream.read(CHUNK)
	    frames.append(data)

	print("* done recording")

	stream.stop_stream()
	stream.close()
	#p.terminate()

	wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
	wf.setnchannels(CHANNELS)
	wf.setsampwidth(p.get_sample_size(FORMAT))
	wf.setframerate(RATE)
	wf.writeframes(b''.join(frames))
	wf.close()

	# Analyze output for volume
	proc = Popen(['ffmpeg', '-i', 'output.wav', '-af', "volumedetect", '-f', 'null', '/dev/null'], stdout=PIPE, stderr=PIPE)
	out, err = proc.communicate()
	#
	#print(out)
	m = re.search('mean_volume:\s(-?\d*(\.\d+)?)', err)
	avgVolume = float(m.group(0).split()[1])
	
	#If the recorded snippet is below -35 dB, count as silence
	if(avgVolume < -35):
		blankImage = np.zeros((81, 150))
		cv2.imwrite("output.bmp", blankImage)
		cv2.imshow("input",cv2.resize(blankImage, (300, 162)))
		cv2.waitKey(20)
		continue

	#CONVERT wav TO IMAGE USING ARSS
	#shell command "./arss -q output.wav output.bmp -min 80 -max 8000 -b 12 -p 150"
	#filename = "03-01-09-01-01-01-99-" + str(Number) + ".bmp"

	call(['./arss', '-q', 'output.wav', 'output.bmp', '-min', '80', '-max', '8000', '-b', '12', '-p', '150'])
	input_image = cv2.imread('output.bmp', cv2.IMREAD_GRAYSCALE).reshape(81, 150, 1)
	#print("MAX: " +  str(np.max(input_image)) )
	cv2.imshow("input",cv2.resize(input_image, (300, 162)))
	cv2.waitKey(20)

	#Number += 1