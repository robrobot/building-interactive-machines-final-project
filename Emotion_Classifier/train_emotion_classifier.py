#!/usr/bin/env python
# Script to train and test a neural network with TF's Keras API for face detection

import os
import sys
import argparse
import datetime
import numpy as np
import tensorflow as tf
import cv2


def load_data_from_npz_file(file_path):
    """
    Load data from npz file
    :param file_path: path to npz file with training data
    :return: input features and target data as numpy arrays
    """
    data = np.load(file_path)
    #print("Data1: " + str(data["one_hot"]))
    #print("Data2: " + str(data["sorted_images"]))
    print("dims onehot: " + str(data["one_hot"].shape))
    print("dims images: " + str(data["sorted_images"].shape))
    return data['sorted_images'], data['one_hot']

def load_data_files(file_path):
    """
    Load data from npz file
    :param file_path: path to npz file with training data
    :return: input features and target data as numpy arrays
    """
    dataList = []

    # Load images into a single array
    for filename in os.listdir(file_path):
    	print str(filename)
    	im = cv2.imread(file_path+"/"+filename, cv2.IMREAD_GRAYSCALE)
    	print "image shape " + str(im.shape)
    	im = im.reshape(1,im.shape[0],im.shape[1])
        dataList.append(im)

    data = np.array(dataList)

    # UNCOMMENT THIS LINE TO SAVE THE FILE AS NPZ
    #np.savez("images.npz", data)
    
    # Load target data into a single array
    #print data.shape
    #print data
    #return data['input'], data['target']
    return data

def compute_normalization_parameters(data):
    """
    Compute normalization parameters (mean, st. dev.)
    :param data: matrix with data organized by rows [N x num_variables]
    :return: mean and standard deviation per variable as row matrices of dimension [1 x num_variables]
    """
    mean = np.mean(data, axis=0).reshape(data.shape[1],data.shape[2],data.shape[3])
    stdev = np.std(data, axis=0).reshape(data.shape[1],data.shape[2],data.shape[3])

    # print(mean)
    # print("divided")
    # print(mean/255.0)


    # cv2.imshow("meandiv",mean/255.0)
    # cv2.imshow("mean",mean)
    # cv2.waitKey();
    return mean, stdev


def normalize_data_per_row(data, mean, stdev):
    """
    Normalize a give matrix of data (samples must be organized per row)
    :param data: input data
    :param mean: mean for normalization
    :param stdev: standard deviation for normalization
    :return: whitened data, (data - mean) / stdev
    """

    # sanity checks!
    assert len(data.shape) == 4, "Expected the input data to be a 4D matrix"
    assert data.shape[1:] == mean.shape, "Data - Mean size mismatch ({} vs {})".format(data.shape[1:], mean.shape)
    assert data.shape[1:] == stdev.shape, "Data - StDev size mismatch ({} vs {})".format(data.shape[1:], stdev.shape)

    normalized_data = (data-mean)/stdev

    return normalized_data

def split_data(input, target, train_percent):
    """
    Split the input and target data into two sets
    :param input: inputs [Nx2] matrix
    :param target: target [Nx1] matrix
    :param train_percent: percentage of the data that should be assigned to training
    :return: train_input, train_target, test_input, test_target
    """
    assert input.shape[0] == target.shape[0], \
        "Number of inputs and targets do not match ({} vs {})".format(input.shape[0], target.shape[0])

    indices = range(input.shape[0])
    np.random.shuffle(indices)

    num_train = int(input.shape[0]*train_percent)
    train_indices = indices[:num_train]
    test_indices = indices[num_train:]

    return input[train_indices, :], target[train_indices,:], input[test_indices,:], target[test_indices,:]

def build_model(input_dim1, input_dim2, input_dim3):
    """
    Build nonlinear NN model with Keras
    :param num_inputs: number of input features for the model
    :return: Keras model
    """
    #print("NUM INPUTS")
    print(input_dim1)
    print(input_dim2)
    print(input_dim3)
    input = tf.keras.layers.Input(shape=(input_dim1, input_dim2, input_dim3), name="inputs")
    
    conv1 = tf.keras.layers.Conv2D(3, (3, 3), padding='same', activation=tf.nn.relu)(input)
    pool1 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = tf.keras.layers.Conv2D(3, (3, 3), padding='same', activation=tf.nn.relu)(pool1)

    drop1 = tf.keras.layers.Dropout(0.2)(conv2) 
    
    conv3 = tf.keras.layers.Conv2D(5, (5, 5), padding='same', activation=tf.nn.relu)(drop1)

    drop2 = tf.keras.layers.Dropout(0.2)(conv3)

    pool2 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(drop2)

    conv4 = tf.keras.layers.Conv2D(10, (2, 2), strides=(2,2), padding='same', activation=tf.nn.relu)(pool2)

    flatten1 = tf.keras.layers.Flatten()(conv4)

    drop3 = tf.keras.layers.Dropout(0.3)(flatten1)

    dense1 = tf.keras.layers.Dense(100, activation='sigmoid')(drop3)

    drop4 = tf.keras.layers.Dropout(0.4)(dense1)

    output = tf.keras.layers.Dense(5, use_bias=True, activation='softmax')(drop4)

    model = tf.keras.models.Model(inputs=input, outputs=output, name="face_model")
    return model
    

def train_model(model, train_input, train_target, val_input, val_target, input_mean, input_stdev,
                epochs=20, learning_rate=0.01, batch_size=16):
    """
    Train the model on the given data
    :param model: Keras model
    :param train_input: train inputs
    :param train_target: train targets
    :param val_input: validation inputs
    :param val_target: validation targets
    :param input_mean: mean for the variables in the inputs (for normalization)
    :param input_stdev: st. dev. for the variables in the inputs (for normalization)
    :param epochs: epochs for gradient descent
    :param learning_rate: learning rate for gradient descent
    :param batch_size: batch size for training with gradient descent
    """
    # normalize
    norm_train_input = normalize_data_per_row(train_input, input_mean, input_stdev)
    norm_val_input = normalize_data_per_row(val_input, input_mean, input_stdev)

    # compile the model: define optimizer, loss, and metrics
    sgd = tf.keras.optimizers.SGD(lr=0.1, decay=1e-6, momentum=0.8, nesterov=True)
    model.compile(optimizer=sgd,
                  loss='categorical_crossentropy',
                  metrics=['binary_accuracy','categorical_accuracy'])

    # tensorboard callback
    #logs_dir = 'logs/log_{}'.format(datetime.datetime.now().strftime("%m-%d-%Y-%H-%M"))
    logs_dir = 'logs/logNew'
    tbCallBack = tf.keras.callbacks.TensorBoard(log_dir=logs_dir, write_graph=True)

    checkpointCallBack = tf.keras.callbacks.ModelCheckpoint(os.path.join(logs_dir,'weights.h5'),
                                                            monitor='val_loss',
                                                            verbose=0,
                                                            save_best_only=True,
                                                            save_weights_only=False,
                                                            mode='auto',
                                                            period=1)

    # do training for the specified number of epochs and with the given batch size
    model.fit(norm_train_input, train_target, epochs=epochs, batch_size=batch_size,
             shuffle=True,
             validation_data=(norm_val_input, val_target),
             callbacks=[tbCallBack, checkpointCallBack])

def test_model(model, test_input, test_target, input_mean, input_stdev, batch_size=60):
    """
    Test a model on a given data
    :param model: trained model to perform testing on
    :param test_input: test inputs
    :param test_target: test targets
    :param input_mean: mean for the variables in the inputs (for normalization)
    :param input_stdev: st. dev. for the variables in the inputs (for normalization)
    :return: predicted targets for the given inputs
    """
    norm_test_input = normalize_data_per_row(test_input, input_mean, input_stdev)

    prediction = model.predict(norm_test_input, batch_size = batch_size) 
    return prediction

def compute_average_L2_error(test_target, predicted_targets):
    """
    Compute the average L2 error for the predictions
    :param test_target: matrix with ground truth targets [N x 1]
    :param predicted_targets: matrix with predicted targets [N x 1]
    :return: average L2 error
    """
    # TO-DO. Complete. Replace the line below with code that actually computes the average L2 error over the targets.
    average_l2_err = np.sqrt(np.sum((test_target - predicted_targets)**2))/test_target.shape[0]

    return average_l2_err

def main(npz_data_file, batch_size, epochs, lr, val, logs_dir, build_fn=build_model):
    """
    Main function that performs training and test on a validation set
    :param npz_data_file: npz input file with training data
    :param batch_size: batch size to use at training time
    :param epochs: number of epochs to train for
    :param lr: learning rate
    :param val: percentage of the training data to use as validation
    :param logs_dir: directory where to save logs and trained parameters/weights
    """

    #input = load_data_files(npz_data_file)

    input, target = load_data_from_npz_file(npz_data_file)
    
    N = input.shape[0]
    assert N == target.shape[0], \
        "The input and target arrays had different amounts of data ({} vs {})".format(N, target.shape[0]) # sanity check!
    print "Loaded {} training examples.".format(N)

    # TODO. Complete. Implement code to train a network for image classification
    # split data into training (70%) and testing (30%)
    all_train_input, all_train_target, test_input, test_target = split_data(input, target, 0.8)

    # split training data into actual training and validation
    train_input, train_target, val_input, val_target = split_data(all_train_input, all_train_target, 0.8)

    # normalize input data and save normalization parameters to file
    mean, stdev = compute_normalization_parameters(train_input)

    # save normalization values
    np.savez(logs_dir+"/normalization_params.npz", mean=mean, stdev=stdev)

    # build the model
    model = build_fn(train_input.shape[1], train_input.shape[2], train_input.shape[3])

    # train the model
    print "\n\nTRAINING..."
    train_model(model, train_input, train_target, val_input, val_target, mean, stdev,
                epochs=epochs, learning_rate=lr, batch_size=batch_size)

    # test the model
    print "\n\nTESTING..."
    predicted_targets = test_model(model, test_input, test_target, mean, stdev)

    # Report average L2 error
    l2_err = compute_average_L2_error(test_target, predicted_targets)
    print "L2 Error on Testing Set: {}".format(l2_err)

if __name__ == "__main__":

    # script arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--epochs", help="number of epochs for training",
                        type=int, default=50)
    parser.add_argument("--batch_size", help="batch size used for training",
                        type=int, default=100)
    parser.add_argument("--lr", help="learning rate for training",
                        type=float, default=1e-3)
    parser.add_argument("--val", help="percent of training data to use for validation",
                        type=float, default=0.8)
    parser.add_argument("--input", help="input file (npz format)",
                        type=str, required=True)
    parser.add_argument("--logs_dir", help="logs directory",
                        type=str, default="")
    parser.add_argument("--load_model", help="path to the model", type=str, default="")
    args = parser.parse_args()

    if len(args.logs_dir) == 0: # parameter was not specified
        print("NO LOGS DIR")
    	args.logs_dir = 'logs/logNew'
        #args.logs_dir = 'logs/log_{}'.format(datetime.datetime.now().strftime("%m-%d-%Y-%H-%M"))

    build_fn = build_model
    if len(args.load_model) > 0:
        build_fn = lambda x, y, z: tf.keras.models.load_model(args.load_model, compile=False)

    if not os.path.isdir(args.logs_dir):
        os.makedirs(args.logs_dir)

    # run the main function
    main(args.input, args.batch_size, args.epochs, args.lr, args.val, args.logs_dir, build_fn=build_fn)
    sys.exit(0)
