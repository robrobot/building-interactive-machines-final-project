#!/usr/bin/env python
# Script to run a model in a windowed approach on an input image
import os
import sys
import argparse
import numpy as np
import tensorflow as tf
import cv2
import copy
import time 
import random
from subprocess import call 
from PIL import Image 
from PIL import ImageFilter 
import pyaudio  
import thread
from timeit import default_timer as timer

from train_emotion_classifier import normalize_data_per_row

global_emotion_tag = "Neutral"
global_talk_tag = "No_Talk"

global_image_file = "blank"
mean = 0
stdev = 0
model = None

neutral_files = None
happy_files = None
sad_files = None
angry_files = None

def load_normalization_params(norm_file):
    """
    Function to load normalization parameters
    :param norm_file: file path
    :return: mean and stdev
    """
    norm_data = np.load(norm_file)
    mean = norm_data['mean']
    stdev = norm_data['stdev']

    # cv2.imshow("mean", mean)
    # cv2.imshow("meandiv", mean/255.0)
    # cv2.imshow("stdev", stdev)
    # cv2.waitKey()

    return mean, stdev

def make_predictions(im, model, mean, stdev):
    """
    Make predictions on the input image
    :param im: input image
    :param model: keras model
    :param mean: mean for input normalization
    :param stdev: st. dev for input normalization
    :return: numpy array of Nx5 dimension, where N is the number of detected faces (>=0) and the 5 columns
    correspond to: min_x, min_y, max_x, max_y, prob. The bounding box of a face is defined the by top-left
    corner (min_x,min_y) and the bottom-right corner (max_x, max_y).
    """
    emotion = model.predict(im.reshape(1,im.shape[0],im.shape[1],im.shape[2]))

    return emotion

def main(input_file, weights_file, norm_file):
    """
    Evaluate the model on the given input data
    :param input_file: npz data
    :param weights_file: h5 file with model definition and weights
    :param norm_file: normalization params
    """

    global global_image_file
    global mean
    global stdev
    global model
    global neutral_files, happy_files, sad_files, angry_files

    global_image_file = input_file

    # load normalization params
    mean, stdev = load_normalization_params(norm_file)
    #print "Loaded normalization parameters from {}".format(norm_file)

    # load the model
    model = tf.keras.models.load_model(weights_file)
    #print "Loaded keras model from {}".format(weights_file)

    # Load sound files
    neutral_files, happy_files, sad_files, angry_files = setup() # run setup and queue all emotion data 

    # START CHARACTER ANIMATION
    run_video_and_update_animation(1) # bring up the intialization and jump into the loop 
    #print("bringing up the screen to display character animation")
    run_video_and_update_animation(0)  

# v--===== SOUND GENERATION CODE =====--V #
def load_rand_sounds(in_emotion_tag, in_user_input_sec,in_neutral_files, in_happy_files, in_sad_files, in_angry_files):

    cut = 30   # further trim snipppets, max is 150, length dimension
    long_time = 5   # time in seconds that a user speaks will inform lenght virtual agent speaks
    short_time = 3

    rand_sounds_list = [] 

    if in_user_input_sec > long_time: #long babble ~ 5 sec
        babble_length = long_time
    else:
        babble_length = short_time

    for i in range(babble_length * 150/cut):
        rand_offset = np.random.randint(0, high=150-cut)

        if in_emotion_tag == "Happy":
            current_rand_emotion = random.choice(in_happy_files)

        elif in_emotion_tag == "Neutral":
            current_rand_emotion = random.choice(in_neutral_files)

        elif in_emotion_tag == "Angry":
            current_rand_emotion = random.choice(in_angry_files)

        elif in_emotion_tag == "Sad":
            current_rand_emotion = random.choice(in_sad_files)

        elif(in_emotion_tag == "Noise"):
            #dont' say anything 
            rand_sounds_list = np.zeros((1,81,150,1))
            break 
        
        current_rand_emotion = current_rand_emotion[:,rand_offset:rand_offset+cut,:]
        rand_sounds_list.append(current_rand_emotion) 

    detected_emotion = in_emotion_tag
    print("The detected Emotion was", detected_emotion)

    return rand_sounds_list 


def blend_snippets_together_and_save(in_rand_sounds_list): 

    #first, aggregate the snippets into one massive array 
    #print("The random sound shape is", np.asarray(in_rand_sounds_list).shape)

    aggregate = np.zeros((81,20,1)) # first little bit will be silence 

    for i in range(len(in_rand_sounds_list)): 
        aggregate = np.append(aggregate, in_rand_sounds_list[i],axis=1)   # stich bmp contents together horizontally 

    # process this list into an array so it can be used with PIL python package to save imag 
    aggregate = aggregate[:,:,0]

    aggregate = np.asarray(aggregate)

    aggregate = Image.fromarray(aggregate.astype('uint8'))

    #apply gaussian blur to smooth between segments 
    aggregate = aggregate.filter(ImageFilter.SMOOTH_MORE)
    aggregate = aggregate.filter(ImageFilter.SMOOTH_MORE)

    # BLUR
    # CONTOUR
    # DETAIL
    # EDGE_ENHANCE
    # EDGE_ENHANCE_MORE
    # EMBOSS
    # FIND_EDGES
    # SHARPEN
    # SMOOTH
    # SMOOTH_MORE

    #mirror image to reverese  
    #reversed_sound = aggregate.transpose(Image.FLIP_LEFT_RIGHT)

    # save image to local directory. every time new emotional speech is generated, this file is overwritten. 
    aggregate.save("gen_emotion_file.bmp")

    # convert to 24-bit colormap so ARSS can be performed on it 
    im = Image.open("gen_emotion_file.bmp").convert("RGB")

    #and resave 
    im.save("gen_emotion_file.bmp")
    return None  


def convert_to_audio(in_blended_sound_image): 

    # use arss to convert the images into audio stream to play to user  

    #call(['./arss', '-q', 'gen_emotion_file.bmp', 'gen_emotion_file.wav', '-min', '80', '-max', '20000', '--pps', '100', '-r', '44100', '-f', '16','--sine'])
    #call(['./Emotion_Classifier/arss', '-q', 'gen_emotion_file.bmp', 'gen_emotion_file.wav', '-s', '-r', '44100', '-min', '80', '-b', '11', '-p', '150', '-f', '16'])
    os.system('./Emotion_Classifier/arss -q gen_emotion_file.bmp gen_emotion_file.wav -s -r 44100 -min 80 -b 11 -p 150 -f 16 >/dev/null 2>&1')


    return None  

def play_sounds(in_neutral_files, in_happy_files, in_sad_files, in_angry_files, emotion, seconds): 


    # wait for an emotion and seconds input: 
    #........................................
    #emotion = "Angry"
    #seconds = 9
    time.sleep(1)

    # load the sounds 
    sounds = load_rand_sounds(emotion, seconds,in_neutral_files, in_happy_files, in_sad_files, in_angry_files)

    # #blend the sounds together to make it sound interesting ans save to directory
    blended_sound_image = blend_snippets_together_and_save(sounds)

    # convert image to .wav file and save it to local directory
    babble = convert_to_audio(blended_sound_image)

    #play the babble sound to the user 
    #os.system('python play_audio.py gen_emotion_file.wav')
    thread.start_new_thread(os.system, ('python Emotion_speech_generator/play_audio.py gen_emotion_file.wav >/dev/null 2>&1',))

    #print("after sound, did script pause?")

    return None  


def setup(): 

    data=np.load('images_1hot_extra.npz') 

    sorted_images = data['sorted_images.npy']
    sorted_filenames = data['sorted_emotion_filenames.npy']

    happy_files = []
    angry_files = []
    sad_files = []
    neutral_files = []

    for i in range(len(sorted_images)): #iterate through all of the images 
        current_im = sorted_images[i]
        current_filenames = sorted_filenames[i]
        
        if (current_filenames[6:8] == "01"):    # neutral 
            neutral_files.append(current_im)
        elif (current_filenames[6:8] == "03"):   # happy 
            happy_files.append(current_im)
        elif (current_filenames[6:8] == "04"):   # sad 
            sad_files.append(current_im)
        elif (current_filenames[6:8] == "05"):   # angry 
            angry_files.append(current_im)
    print('finished sorting')
    return neutral_files, happy_files, sad_files, angry_files 

# ^--===== END SOUND GENERATION CODE =====--^


# v--===== CHARACTER ANIMATION CODE =====--v #

def init_emotion_tag(): # initialize character with neutral expression and not talking
    return global_emotion_tag

def init_talk_state():
    return global_talk_tag

def get_emotion_tag(): 

    global global_image_file
    # load the input image
    input_image = cv2.imread(global_image_file, cv2.IMREAD_GRAYSCALE)

    # If trying to read right when ARSS is changing file, assume same emotion
    if(input_image is None):
        print "COULD NOT READ"
        return "No_Update"

    if(np.max(input_image) == 0):
        print "DETECTED SILENCE"
        return "Silence"

    input_image = input_image.reshape(81, 150, 1)
    #cv2.imshow("input_image",input_image)
    assert input_image is not None, "Failed to load input image: {}".format(input_file)
    #print "Loaded image with dimensions {}".format(input_image.shape)


    # make predictions on windows within the image
    detected_emotion = make_predictions(input_image, model, mean, stdev)[0]

    highest_emotion = np.argmax(detected_emotion)

    if(highest_emotion == 0):
        global_emotion_tag = "Neutral"
    elif(highest_emotion == 1):
        global_emotion_tag = "Happy"
    elif(highest_emotion == 2):
        global_emotion_tag = "Sad"
    elif(highest_emotion == 3):
        global_emotion_tag = "Angry"
    elif(highest_emotion == 4):
        global_emotion_tag = "Noise"

    detected_emotion2 = detected_emotion.copy()
    detected_emotion2[highest_emotion] = 0

    highest_emotion2 = np.argmax(detected_emotion2)

    if(highest_emotion2 == 0):
        global_emotion_tag2 = "Neutral"
    elif(highest_emotion2 == 1):
        global_emotion_tag2 = "Happy"
    elif(highest_emotion2 == 2):
        global_emotion_tag2 = "Sad"
    elif(highest_emotion2 == 3):
        global_emotion_tag2 = "Angry"
    elif(highest_emotion2 == 4):
        global_emotion_tag2 = "Noise"


    print("DETECTED: " + str(global_emotion_tag) + " (" + "{:.2f}".format(detected_emotion[highest_emotion] * 100) + "%) SECOND: " + str(global_emotion_tag2) + " (" + "{:.2f}".format(detected_emotion2[highest_emotion2] * 100) + "%)") 

    # If we detect noise, just output the 2nd-highest score
    if(highest_emotion == 4):
        global_emotion_tag = global_emotion_tag2   

    return global_emotion_tag

def get_talk_state(talk_timer): 
    # write the code to interface with RNN babbling generation to move character mouth only when speak
    print(talk_timer)
    if(talk_timer > 0):
        return "Talk"
    else:
        return "No_Talk"


def load_face_animation(string, string_2):
    if string == "Angry" and string_2 == "No_Talk":
        cap = cv2.VideoCapture('virtual_character_scripts/Angry_No_Talk_TRIMMED.mp4')
    elif string == "Angry" and string_2 == "Talk": 
        cap = cv2.VideoCapture('virtual_character_scripts/Angry_babble_TRIMMED.mp4')
    elif string == "Happy" and string_2 == "No_Talk":
        cap = cv2.VideoCapture('virtual_character_scripts/Happy_No_Talk_TRIMMED.mp4')
    elif string == "Happy" and string_2 == "Talk": 
        cap = cv2.VideoCapture('virtual_character_scripts/Happy_babble_TRIMMED.mp4')
    elif string == "Sad" and string_2 == "No_Talk":
        cap = cv2.VideoCapture('virtual_character_scripts/Sad_No_Talk_TRIMMED.mp4')
    elif string == "Sad" and string_2 == "Talk": 
        cap = cv2.VideoCapture('virtual_character_scripts/Sad_babble_TRIMMED.mp4')
    elif string == "Neutral" and string_2 == "No_Talk":
        cap = cv2.VideoCapture('virtual_character_scripts/Neutral_No_Talk_TRIMMED.mp4')
    elif string == "Neutral" and string_2 == "Talk": 
        cap = cv2.VideoCapture('virtual_character_scripts/Neutral_babble_TRIMMED.mp4')
    else:
        cap = None 

    return cap 

def run_video_and_update_animation(in_init_or_not): # input is 1 if its first time its called. 0 otherwisze

    test_emotion_timer = timer()
    talk_timer = timer()
    input_length = 0
    detected_emotions = [0, 0, 0, 0, 0]


    if in_init_or_not == 1: 

        emotion_tag = init_emotion_tag() # initialize emotion tag and talk state with neutral, no talk
        talk_state = init_talk_state() 
        cap = load_face_animation(emotion_tag, talk_state)
        time.sleep(0.5) # delay while the window and character intialize 


    else: # we're past initialization, now lets continuously update the face in a loop with different emotions 

        old_emotion_tag = init_emotion_tag()
        old_talk_state = init_talk_state()
        last_known_emotion = old_emotion_tag

        # Start off old and new states equal
        emotion_tag = old_emotion_tag
        talk_state = old_talk_state

        while(True): 
            #if(old_emotion_tag != "Silence"):
            newcap = load_face_animation(last_known_emotion, talk_state) # update the face animation based on present emotion
            if(newcap != None):
                cap = newcap

            frame_counter = 0        

            # Check if camera opened successfully
            if (cap.isOpened()== False): 
              print("Error opening video file")

            old_emotion_tag = emotion_tag
            old_talk_state = talk_state

            while(True):   

                # Check for emotion every 1 second, but only when sound is not playing
                if((timer() - test_emotion_timer) >= 1.0 and (timer() - talk_timer) >= 0):
                    emotion_tag = get_emotion_tag() # 1/sec search for emotion tag to see if it's changed
                    test_emotion_timer = timer()

                    if(emotion_tag != "Silence"):
                        input_length += 1
                        last_known_emotion = emotion_tag
                        if(emotion_tag == "Neutral"):
                            detected_emotions[0] += 1
                        elif(emotion_tag == "Happy"):
                            detected_emotions[1] += 1
                        elif(emotion_tag == "Sad"):
                            detected_emotions[2] += 1
                        elif(emotion_tag == "Angry"):
                            detected_emotions[3] += 1
                        elif(emotion_tag == "Noise"):
                            detected_emotions[4] += 1

                # Move to "No_Talk" state when our timer says the sound is done
                if((timer() - talk_timer) >= 0):
                    if(talk_state == "Talk"):
                        print("STOP TALKING")
                        talk_state = "No_Talk"
                        #emotion_tag = last_known_emotion
                        #print("EMOTION RESET TO: " + str(emotion_tag))
                else:
                    talk_state = "Talk"

                # if the emotion tag and talk_state did not change (or no new data was read), continue animation
                if(emotion_tag == "No_Update" or emotion_tag == "Noise" or old_emotion_tag == emotion_tag and old_talk_state == talk_state):  

                    ret, frame = cap.read() 
                    frame_counter += 1 # iterate frame counter 
                    if ret == True:
                     
                        # Display the resulting frame
                        #cv2.imshow('Frame',frame)
                        cv2.imshow('Frame',cv2.resize(frame, (480, 270))) #shrink image for testing
                        cv2.waitKey(30)
                        #print("displaying frame",frame_counter, "of", cap.get(cv2.CAP_PROP_FRAME_COUNT))

                else:   # if old tags and current are not the same, there's been a change, so break the while loop and redo the load_face_animation              
                    
                    # If user was talking and now we hear silence, start talking
                    if(emotion_tag == "Silence" and old_emotion_tag != "Silence" and (timer() - talk_timer) >= 0):
                        most_detected_emotion = np.argmax(detected_emotions)
                        if(most_detected_emotion == 0):
                            most_detected_emotion = "Neutral"
                        elif(most_detected_emotion == 1):
                            most_detected_emotion = "Happy"
                        elif(most_detected_emotion == 2):
                            most_detected_emotion = "Sad"
                        elif(most_detected_emotion == 3):
                            most_detected_emotion = "Angry"
                        elif(most_detected_emotion == 4):
                            most_detected_emotion = "Noise"

                        play_sounds(neutral_files, happy_files, sad_files, angry_files, most_detected_emotion, input_length)
                        print("Starting to talk")
                        talk_state = "Talk"
                        talk_timer = timer() + 3.0 # Wait 2 seconds before stopping talk animation
                        last_known_emotion = old_emotion_tag
                        input_length = 0

                    print('Change in state from:', old_emotion_tag, old_talk_state,'to:', emotion_tag, talk_state)

                    break 

                #If the last frame is reached the video has ended, so reset the frame to 0 in reset the counter
                if frame_counter == cap.get(cv2.CAP_PROP_FRAME_COUNT):
                    frame_counter = 0 
                    cap.set(cv2.CAP_PROP_POS_FRAMES, 0)


            # Press Q on keyboard to  destroy character  
            if cv2.waitKey(25) & 0xFF == ord('q'):
                cap.release()
                cv2.destroyAllWindows()

# ^--==== END CHARACTER ANIMATION CODE ====--^ #



if __name__ == "__main__":

    # script arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", help="input image",
                        type=str, required=True)
    parser.add_argument("--logs_dir", help="logs directory",
                        type=str, required=True)
    parser.add_argument("--norm_filename", help="name for the normalization params file",
                        type=str, default="normalization_params.npz")
    parser.add_argument("--weights_filename", help="name for the weights file",
                        type=str, default="weights.h5")
    args = parser.parse_args()

    weights_path = os.path.join(args.logs_dir, args.weights_filename)
    norm_path = os.path.join(args.logs_dir, args.norm_filename)

    # run the main function
    main(args.input, weights_path, norm_path)
    sys.exit(0)