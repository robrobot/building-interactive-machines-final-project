#!/bin/bash
for file in $(find -type f -name "*.avi"); do
	echo "Processing $file";

	#filename="${file%.*}"

	#convert to audio only
	#ffmpeg -hide_banner -loglevel error -i $file $filename".wav"
	#rm $file

	# # remove silence from both ends of audio files
	# ffmpeg -hide_banner -loglevel error -ss 0.1 -i $file -af "silenceremove=1:0:-50dB:detection=peak, areverse, silenceremove=1:0:-50dB:detection=peak, areverse" tmp.wav
	
	# # re-encode file so ARSS likes it
	# sox tmp.wav tmp2.wav
	
	# # delete the original and rename the fixed version
	# rm $file
	# rm tmp.wav
	# mv tmp2.wav $file

	# # split into 1-second chunks
	# filename="${file%.*}"

	# sox $file $filename"-"%n.wav trim 0 1 : newfile : restart	# Split every 1s until end
	# sox $file $filename"-"09.wav trim -1 -0						# Also get the last second

	# rm $file
	# #filename="${file%.*}"
	# #./arss -q $file $filename".bmp" -min 80 -max 8000 -b 12 -p 150
	# #./arss $filename".bmp" $filename"OUT.wav" -min 80 -max 8000 -p 150 -r 44100 -f 16 --sine
done

# # Rename folders
# a=25
# for dir in $(find -type d); do
# 	if [[ "$dir" == "." ]]; then
# 		continue
# 	fi
# 	mv $dir $a
# 	let a=a+1
# done

# Rename all wav files to expected format
for file in $(find -type f -name "*.wav"); do
	echo "Processing $file";

	filename="${file%.*}"
	filename="${filename##*/}"
	utterance="${filename:2:1}"

	pathname="${file%/*}"
#	echo $pathname
	actor="${pathname:2:2}"

	if [[ "$file" == *"sa"* ]]; then	# SAD
		mv $file $pathname"/"03-01-04-02-0$utterance-01-$actor".wav"
	fi

	if [[ "$file" == *"an"* ]]; then	# ANGRY
		mv $file $pathname"/"03-01-05-02-0$utterance-01-$actor".wav"
	fi

	if [[ "$file" == *"ha"* ]]; then	# HAPPY, but sounds NEUTRAL (baaaaad actors)
		mv $file $pathname"/"03-01-01-02-0$utterance-01-$actor".wav"
	fi

	if [[ "$file" == *"su"* ]]; then	# SURPRISED, but sounds HAPPY (so soo bad)
		mv $file $pathname"/"03-01-03-02-0$utterance-01-$actor".wav"
	fi

	if [[ "$file" == *"di"* ]]; then	# DISGUSTED
		mv $file $pathname"/"03-01-07-02-0$utterance-01-$actor".wav"
	fi

	if [[ "$file" == *"fe"* ]]; then	# FEARFUL
		mv $file $pathname"/"03-01-06-02-0$utterance-01-$actor".wav"
	fi

done

# echo "purging small files"
# for file in $(find -type f -name "*.wav"); do

# 	# delete the clips that were under 1s
# 	soundlength=$(soxi -D $file)
# 	if [ 1 -eq $(echo "$soundlength < 1" | bc ) ]; then
# 		echo "$file is small. Deleting.";
# 		rm $file
# 	fi;

# done

# echo "converting to images"
# for file in $(find -type f -name "*.wav"); do

# 	# Convert all remaining files into image files, from 80 - 8000 Hz, 12 px per octave, 150px per second
# 	filename="${file%.*}"
# 	./arss -q $file $filename".bmp" -min 80 -max 8000 -b 12 -p 150

# done

# find -type f -name "*.bmp" -exec mv "{}" ./Images \;